
    <div class="footer">
        <div class="container">
            <div class="footer__logo"><img src="<?php echo base_url('assets/frontend') ?>/assets/nbls-logo-icon.svg">
            </div>
            <div class="footer__content">
                <div class="footer__content-address"><br>
                    <br><span>Country(ES) NOBLES COIN, SOCIEDAD LIMITADA <br> <strong>Business license</strong> :
                        B09855933</span><br><span>Av. del Dr. Federico Rubio y Galí, 19, 28039, Madrid <br> Ref.
                        catastral 9882703VK3798D0259AA</span><br>
                        
                        
                </div>
                <div class="footer__content-contact"> 
                    <span class="contact">Contact</span><br>
                    <span>
                        <strong>Cs Center</strong> +34-6229-5816-5 
                        <br> 24-hour customer center operation (Korea,
                        Spain)
                    </span><br>
                    <span>
                        <strong>E-mail</strong> : nbls8234@gmail.com
                    </span>
                </div>
                <div class="footer__content-menu"> 
                    <span>Menu</span><br>
                    <a>NBLS Docs</a><br>
                    <a>Media</a><br>
                    <a>NBLS Token</a><br>
                    <a>About Us</a><br>
                    <a>Ecosystem</a><br>
                    <a>Analytics</a>
                </div>
                <div class="footer__content-findus"> <span>Find Us</span><br>
                
                <a href="https://twitter.com/NBLS12" target="_blank"> 
                    <img src="<?php echo base_url('assets/frontend') ?>/assets/twitter-nblh.svg">
                    <span>twitter</span>
                </a><br>
                <a href="https://www.youtube.com/hashtag/%EB%85%B8%EB%B8%94%EB%A0%88%EC%8A%A4%EC%BD%94%EC%9D%B8" target="_blank">
                        <img src="<?php echo base_url('assets/frontend') ?>/assets/youtube-nblh.svg">
                        <span>youtube</span>
                </a>
                </div>
            </div>
            <div class="footer__copyright"><span>Copyright © 2022 NBLS. All Right Reserved.</span></div>
        </div>
    </div>
    <script nonce="some-nonce">
        window.onscroll = function () {
            if (window.pageYOffset > 50 && window.innerWidth > 768) {
                document.querySelector('.navbar--dashboard').classList.add("active");
            } else {
                document.querySelector('.navbar--dashboard').classList.remove("active");
            }
        };

        $(document).ready(function () {

            $(".navbar__mobile").click(function () {
                $(".navbar__container").toggleClass("show")
            })

            $("#dropdown").click(function () {
                $(".dropdown__menu").toggleClass('on')
            })
        });
    </script>
</body>

</html>