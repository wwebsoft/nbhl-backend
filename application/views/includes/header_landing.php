<!DOCTYPE html>
<html lang="en-GB">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.10/clipboard.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend') ?>/main.css">
    <title>NBLS</title>
</head>

<body>
    <div class="navbar navbar--dashboard">
        <div class="container">
            <a class="navbar__logo--dashboard" href="<?php echo base_url() ?>">
                <img src="<?php echo base_url('assets/frontend') ?>/assets/nbls-logo-icon.svg" alt="">
            </a>
            <div class="navbar__container">
                <div class="navbar__container__menu--dashboard">
                    <a class="menu-item" href="#">
                        <span>NBLS Docs</span>
                    </a>
                    <a class="menu-item" href="#">
                        <span>Media</span>
                    </a>
                    <a class="menu-item" href="#">
                        <span>NBLS Token</span>
                    </a>
                    <a class="menu-item" href="#">
                        <span>About Us</span>
                    </a>
                    <a class="menu-item" href="#">
                        <span>Ecosystem</span>
                    </a>
                    <a class="menu-item" href="#">
                        <span>Analytics</span>
                    </a>
                </div>
                <div class="navbar__container__right-btn--dashboard">
                    <a class="nav-right-btn" href="https://twitter.com/NBLS12" target="_blank">
                        <img src="<?php echo base_url('assets/frontend') ?>/assets/twitter-nblh.svg" alt="">
                    </a>
                    <a class="nav-right-btn border" href="tel:34-6229-5816-5"> 
                        <img src="<?php echo base_url('assets/frontend') ?>/assets/call.svg"
                            alt="">
                        <span>+34-6229-5816-5</span>
                    </a>
                    <div class="dropdown" id="dropdown">
                        <span class="nav-right-btn border" href=""> 
                            <img src="<?php echo base_url('assets/frontend') ?>/assets/UK-lang.svg"
                                alt="">
                            <span>ENG</span>
                            <img src="<?php echo base_url('assets/frontend') ?>/assets/arrow-down-lang.svg">
                        </span>
                        <a class="nav-right-btn dropdown__menu" href="">
                            <div class="dropdown__menu__items">
                                <div class="dropdown__menu__items__head"><img
                                        src="<?php echo base_url('assets/frontend') ?>/assets/UK-lang.svg"
                                        alt=""><span>English</span></div><img
                                    src="<?php echo base_url('assets/frontend') ?>/assets/check-circle.svg">
                            </div>
                            <div class="dropdown__menu__items">
                                <div class="dropdown__menu__items__head"><img
                                        src="<?php echo base_url('assets/frontend') ?>/assets/KR-lang.svg"
                                        alt=""><span>Korea</span></div><img
                                    src="<?php echo base_url('assets/frontend') ?>/assets/check-circle.svg">
                            </div>
                        </a></div>
                </div>
            </div>
            <div class="navbar__mobile"><img src="<?php echo base_url('assets/frontend') ?>/assets/menu.svg"></div>
        </div>
    </div>