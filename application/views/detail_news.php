<?php include('includes/header_landing.php'); ?>


<div class="header">
    <div class="container">
        <div class="display">
            <h1 class="display__tittle"><?php echo $article_detail[0]->title ?></h1>
            <p class="display__paragraf">By Dwinawan</p>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="content__head">
            <img class="content__head__img" src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png" alt="">
            <div class="content__head__news news">
                <div class="news__socialmedia">
                    <img src="<?php echo base_url('assets/frontend') ?>/assets/img-f.svg" alt="">
                    <img src="<?php echo base_url('assets/frontend') ?>/assets/img-t.svg"
                        alt="">
                    <img src="<?php echo base_url('assets/frontend') ?>/assets/img-l.svg" alt="">
                    <img src="<?php echo base_url('assets/frontend') ?>/assets/img-c.svg" alt="" id="copyMe" data-clipboard-text="<?php echo current_url(); ?>">
                </div>
                <div class="news__paragraf">
                    
                    <?php echo $article_detail[0]->description ?>

                    <p class="news__paragraf__subcontent">
                        <br/>
                        <span><?php echo $article_detail[0]->publish ?></span>
                        <span>.<?php echo $article_detail[0]->category_name ?></span>
                        <span>.<?php echo $article_detail[0]->readTime ?> min read</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="interest">
    <div class="container">
        <div class="interest__tittle">Something you might interest</div>
        <div class="interest__container">
            <div class="interest__container__card card">
                <?php
                            foreach($articles as $k => $v){
                                ?>
                    <a class="card-items" href="<?php echo base_url('detail/'.$v['id'])?>">
                        <img class="card-items-image" src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png"
                            alt="">
                        <div class="card-items_media">
                            <div class="card-items_media-content"><?php echo $v['publish'];?></div>
                            <div class="card-items_media-tittle"><?php echo $v['title'];?></div>
                        </div>
                    </a>
                <?php
                        }
                    ?>
            </div>
        </div>
    </div>
</div>

<script>
    new ClipboardJS('#copyMe');
</script>
<?php include('includes/footer_landing.php'); ?>