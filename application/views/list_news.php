<?php include('includes/header_landing.php'); ?>

<div class="header">
    <div class="container">
        <div class="display">
            <h1 class="display__tittle">Notice</h1>
            <div class="display__btn-container input">
                <div class="input__wrap">
                    <input class="input__wrap__fill" type="search" placeholder="Search topic..."><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/search-icon-listnews.svg">
                </div>
                <div class="input__sort"> <span>Sort</span><img src="<?php echo base_url('assets/frontend') ?>/assets/arrow-down-lang.svg"></div>
            </div>
        </div>
    </div>
</div>
<div class="recomendation">
    <div class="container">
        <div class="recomendation__tittle">Our Recomendation</div>
        <div class="recomendation__container">
            <div class="recomendation__container__card card">
                <div class="card__wrapp">
                    <div class="card__wrapp-items">
                        <img class="card__wrapp-items-image"
                            src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png" alt="">
                        <div class="card__wrapp-items_media">
                            <div class="card__wrapp-items_media-content"><?php echo $article_trends[0]['publish']; ?></div>
                            <div class="card__wrapp-items_media-tittle"><?php echo $article_trends[0]['title']; ?></div>
                            <div class="card__wrapp-items_media-content"><?php echo $article_trends[0]['description']; ?></div>
                            <a href="<?php echo base_url('detail/'.$article_trends[0]['id']); ?>">Read more</a>
                        </div>
                    </div>
                </div>
                <div class="card__wrapp">

                    <?php
                        foreach($article_trends as $k => $v){
                            if($k != 0){
                    ?>

                        <a class="card__wrapp__container" href="<?php echo base_url('detail/'.$v['id'])?>">
                            <div class="card__wrapp__container-list">
                                <img class="card__wrapp__container-list-image"
                                    src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png" alt="">
                                <div class="card__wrapp__container-list__tittle">
                                    <div class="card__wrapp__container-list__tittle__date"><?php echo $v['publish'];?></div>
                                    <div class="card__wrapp__container-list__tittle__content"><?php echo $v['title'];?></div>
                                </div>
                            </div>
                        </a>

                    <?php
                            }}
                    ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="newest">
    <div class="container">
        <div class="newest__tittle">Newest</div>
        <div class="newest__container">
            <div class="newest__container__card card">
                <?php
                            foreach($articles as $k => $v){
                                ?>
                    <a class="card-items" href="<?php echo base_url('detail/'.$v['id'])?>">
                        <img class="card-items-image" src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png"
                            alt="">
                        <div class="card-items_media">
                            <div class="card-items_media-content"><?php echo $v['publish'];?></div>
                            <div class="card-items_media-tittle"><?php echo $v['title'];?></div>
                        </div>
                    </a>
                <?php
                        }
                    ?>
            </div>
        </div>
    </div>
</div>
<div class="trending">
    <div class="container">
        <div class="trending__tittle">Trending</div>
        <div class="trending__container">
            <div class="trending__container__card card">
                <div class="card__wrapp">
                    <?php
                        foreach($article_trends as $k => $v){
                            ?>
                        <div class="card__wrapp__container">
                            <div class="card__wrapp__container-list">
                                <img class="card__wrapp__container-list-image" src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png" alt="">
                                <div class="card__wrapp__container-list__tittle">
                                    <div class="card__wrapp__container-list__tittle__date"><?php echo $v['publish'];?></div>
                                    <div class="card__wrapp__container-list__tittle__content"><?php echo $v['title'];?></div>
                                    <div class="card__wrapp__container-list__tittle__date"><?php echo $v['description'];?></div>
                                    <div class="card__wrapp__container-list__tittle__readmore">
                                        <a href="<?php echo base_url('detail/'.$v['id'])?>">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                            }
                        ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('includes/footer_landing.php'); ?>