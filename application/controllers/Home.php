<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Home extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->load->model('article_model');
        $this->global['articles'] = $this->article_model->getAll(3);

        $this->load->view('home',$this->global);
    }


    public function detail($p){
        $this->load->model('article_model');
        $this->global['article_detail'] = $this->article_model->getDetail($p);
        $this->global['articles'] = $this->article_model->getAll(3);

        $this->load->view('detail_news',$this->global);
    }

    public function list(){
        $this->load->model('article_model');
        $this->global['articles'] = $this->article_model->getAll(3);
        $this->global['article_trends'] = $this->article_model->getAll(6,true);

        $this->load->view('list_news',$this->global);
    }

    public function search($q){
        $this->load->model('article_model');
        $this->global['articles'] = $this->article_model->getSearch($q);

        $this->load->view('search_list',$this->global);
    }
    

}

?>